package org.romainb.parisportifapi.controller;

import org.romainb.parisportifapi.entity.Competition;
import org.romainb.parisportifapi.repository.CompetitionRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CompetitionController {
    private final CompetitionRepository competitionRepository;

    public CompetitionController(CompetitionRepository competitionRepository) {
        this.competitionRepository = competitionRepository;
    }

    @GetMapping("/competitions")
    public @ResponseBody Iterable<Competition> getAllCompetitions() {
        return this.competitionRepository.findAll();
    }
}
