package org.romainb.parisportifapi.controller;

import org.romainb.parisportifapi.entity.Bet;
import org.romainb.parisportifapi.entity.BetCategory;
import org.romainb.parisportifapi.entity.BetSaved;
import org.romainb.parisportifapi.repository.BetCategoryRepository;
import org.romainb.parisportifapi.repository.BetRepository;
import org.romainb.parisportifapi.repository.BetSavedRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BettingController {
    private final BetRepository betRepository;
    private final BetCategoryRepository betCategoryRepository;
    private final BetSavedRepository betSavedRepository;

    public BettingController(
            BetRepository betRepository,
            BetCategoryRepository betCategoryRepository,
            BetSavedRepository betSavedRepository
    ) {
        this.betRepository = betRepository;
        this.betCategoryRepository = betCategoryRepository;
        this.betSavedRepository = betSavedRepository;
    }

    @GetMapping("/bets")
    public @ResponseBody Iterable<Bet> getAllBets() {
        return this.betRepository.findAll();
    }

    @GetMapping("/bet-categories")
    public @ResponseBody Iterable<BetCategory> getAllBetCategories() {
        return this.betCategoryRepository.findAll();
    }

    @GetMapping("/bets-saved")
    public @ResponseBody Iterable<BetSaved> getAllBetsSaved() {
        return this.betSavedRepository.findAll();
    }
}
