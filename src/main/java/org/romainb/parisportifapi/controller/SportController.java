package org.romainb.parisportifapi.controller;

import org.romainb.parisportifapi.entity.Sport;
import org.romainb.parisportifapi.repository.SportRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SportController {
    private final SportRepository sportRepository;

    public SportController(SportRepository sportRepository) {
        this.sportRepository = sportRepository;
    }

    @GetMapping("/sports")
    public @ResponseBody Iterable<Sport> getAllSports() {
        return this.sportRepository.findAll();
    }
}
