package org.romainb.parisportifapi.controller;

import org.romainb.parisportifapi.entity.Language;
import org.romainb.parisportifapi.entity.User;
import org.romainb.parisportifapi.entity.Wallet;
import org.romainb.parisportifapi.repository.LanguageRepository;
import org.romainb.parisportifapi.repository.UserRepository;
import org.romainb.parisportifapi.repository.WalletRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {
    private final UserRepository userRepository;
    private final WalletRepository walletRepository;
    private final LanguageRepository languageRepository;

    public AuthenticationController(
            UserRepository userRepository,
            WalletRepository walletRepository,
            LanguageRepository languageRepository
    ) {
        this.userRepository = userRepository;
        this.walletRepository = walletRepository;
        this.languageRepository = languageRepository;
    }

    @GetMapping("/users")
    public @ResponseBody Iterable<User> getAllUsers() {
        return this.userRepository.findAll();
    }

    @GetMapping("/wallets")
    public @ResponseBody Iterable<Wallet> getAllWallets() {
        return this.walletRepository.findAll();
    }

    @GetMapping("/languages")
    public @ResponseBody Iterable<Language> getAllLanguages() {
        return this.languageRepository.findAll();
    }
}
