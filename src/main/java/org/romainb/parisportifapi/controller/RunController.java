package org.romainb.parisportifapi.controller;

import org.romainb.parisportifapi.entity.Location;
import org.romainb.parisportifapi.entity.Run;
import org.romainb.parisportifapi.repository.LocationRepository;
import org.romainb.parisportifapi.repository.RunRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RunController {
    private final RunRepository runRepository;
    private final LocationRepository locationRepository;

    public RunController(
            RunRepository runRepository,
            LocationRepository locationRepository
    ) {
        this.runRepository = runRepository;
        this.locationRepository = locationRepository;
    }

    @GetMapping("/runs")
    public @ResponseBody Iterable<Run> getAllRuns() {
        return this.runRepository.findAll();
    }

    @GetMapping("/locations")
    public @ResponseBody Iterable<Location> getAllLocations() {
        return this.locationRepository.findAll();
    }
}
