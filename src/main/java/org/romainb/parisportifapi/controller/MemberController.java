package org.romainb.parisportifapi.controller;

import org.romainb.parisportifapi.entity.Member;
import org.romainb.parisportifapi.entity.MemberRole;
import org.romainb.parisportifapi.entity.MemberStatus;
import org.romainb.parisportifapi.repository.MemberRepository;
import org.romainb.parisportifapi.repository.MemberRoleRepository;
import org.romainb.parisportifapi.repository.MemberStatusRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberController {
    private final MemberRepository memberRepository;
    private final MemberRoleRepository memberRoleRepository;
    private final MemberStatusRepository memberStatusRepository;

    public MemberController(
            MemberRepository memberRepository,
            MemberRoleRepository memberRoleRepository,
            MemberStatusRepository memberStatusRepository
    ) {
        this.memberRepository = memberRepository;
        this.memberRoleRepository = memberRoleRepository;
        this.memberStatusRepository = memberStatusRepository;
    }

    @GetMapping("/members")
    public @ResponseBody
    Iterable<Member> getAllMembers() {
        return this.memberRepository.findAll();
    }

    @GetMapping("/member-roles")
    public @ResponseBody
    Iterable<MemberRole> getAllMemberRoles() {
        return this.memberRoleRepository.findAll();
    }

    @GetMapping("/member-status")
    public @ResponseBody
    Iterable<MemberStatus> getAllMemberStatus() {
        return this.memberStatusRepository.findAll();
    }
}
