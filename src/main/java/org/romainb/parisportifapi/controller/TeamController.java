package org.romainb.parisportifapi.controller;

import org.romainb.parisportifapi.entity.Team;
import org.romainb.parisportifapi.repository.TeamRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeamController {
    private final TeamRepository teamRepository;

    public TeamController(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @GetMapping("/teams")
    public @ResponseBody
    Iterable<Team> getAllTeams() {
        return this.teamRepository.findAll();
    }
}
