package org.romainb.parisportifapi.controller;

import org.romainb.parisportifapi.entity.Billing;
import org.romainb.parisportifapi.repository.BillingRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {
    private final BillingRepository billingRepository;

    public AccountController(BillingRepository billingRepository) {
        this.billingRepository = billingRepository;
    }

    @GetMapping("/billings")
    public @ResponseBody
    Iterable<Billing> getAllBillings() {
        return this.billingRepository.findAll();
    }
}
