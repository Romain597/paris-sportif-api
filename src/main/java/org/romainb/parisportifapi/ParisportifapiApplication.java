package org.romainb.parisportifapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParisportifapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParisportifapiApplication.class, args);
	}

}
