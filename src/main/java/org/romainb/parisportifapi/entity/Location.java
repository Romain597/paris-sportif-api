package org.romainb.parisportifapi.entity;

import javax.persistence.*;

@Entity(name="Location")
@Table(name="location")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String place;
    private String timeZone;
    private String country;

    public Integer getId() {
        return id;
    }

    public String getPlace() {
        return place;
    }

    public Location setPlace(String place) {
        this.place = place;
        return this;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public Location setTimeZone(String timeZone) {
        this.timeZone = timeZone;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Location setCountry(String country) {
        this.country = country;
        return this;
    }
}
