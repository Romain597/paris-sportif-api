package org.romainb.parisportifapi.entity;

import javax.persistence.*;

@Entity(name="MemberStatus")
@Table(name="member_status")
public class MemberStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public MemberStatus setName(String name) {
        this.name = name;
        return this;
    }
}
