package org.romainb.parisportifapi.entity;


import javax.persistence.*;

@Entity(name="BetCategory")
@Table(name="bet_category")
public class BetCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String description = null;
    private Boolean allowDraw = false;
    private String target;
    private boolean onCompetition;
    public static final String TEAM_TYPE = "teams";
    public static final String MEMBER_TYPE = "members";
    public static final String[] TARGET_TYPES = { TEAM_TYPE, MEMBER_TYPE };

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BetCategory setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public BetCategory setDescription(String description) {
        this.description = description;
        return this;
    }

    public Boolean getAllowDraw() {
        return allowDraw;
    }

    public BetCategory setAllowDraw(Boolean allowDraw) {
        this.allowDraw = allowDraw;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public BetCategory setTarget(String target) {
        this.target = target;
        return this;
    }

    public boolean getOnCompetition() {
        return onCompetition;
    }

    public BetCategory setOnCompetition(boolean onCompetition) {
        this.onCompetition = onCompetition;
        return this;
    }
}
