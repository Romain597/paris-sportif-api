package org.romainb.parisportifapi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.*;

@Entity(name="Bet")
@Table(name="bet")
public class Bet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String designation;
    private Integer amount;
    private String odds;
    private Boolean isWinning = null;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    @JsonBackReference
    private User user;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="bet_category_id")
    private BetCategory betCategory;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="competition_id")
    private Competition competition;
    @ManyToOne(fetch = FetchType.LAZY,
            optional = true)
    @JoinColumn(name="run_id",
            nullable = true)
    private Run run = null;
    @ManyToOne(fetch = FetchType.LAZY,
            optional = true)
    @JoinColumn(name="team_id",
            nullable = true)
    private Team team = null;
    @ManyToOne(fetch = FetchType.LAZY,
            optional = true)
    @JoinColumn(name="team_member_id",
            nullable = true)
    private Member teamMember = null;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date betDate;

    public Integer getId() {
        return id;
    }

    public String getDesignation() {
        return designation;
    }

    public Bet setDesignation(String designation) {
        this.designation = designation;
        return this;
    }

    public Integer getAmount() {
        return amount;
    }

    public Bet setAmount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public String getOdds() {
        return odds;
    }

    public Bet setOdds(String odds) {
        this.odds = odds;
        return this;
    }

    public Boolean getIsWinning() {
        return isWinning;
    }

    public Bet setIsWinning(Boolean isWinning) {
        this.isWinning = isWinning;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Bet setUser(User user) {
        this.user = user;
        return this;
    }

    public BetCategory getBetCategory() {
        return betCategory;
    }

    public Bet setBetCategory(BetCategory betCategory) {
        this.betCategory = betCategory;
        return this;
    }

    public Competition getCompetition() {
        return competition;
    }

    public Bet setCompetition(Competition competition) {
        this.competition = competition;
        return this;
    }

    public Run getRun() {
        return run;
    }

    public Bet setRun(Run run) {
        this.run = run;
        return this;
    }

    public Team getTeam() {
        return team;
    }

    public Bet setTeam(Team team) {
        this.team = team;
        return this;
    }

    public Member getTeamMember() {
        return teamMember;
    }

    public Bet setTeamMember(Member teamMember) {
        this.teamMember = teamMember;
        return this;
    }

    public Date getBetDate() {
        return betDate;
    }

    public Bet setBetDate(Date betDate) {
        this.betDate = betDate;
        return this;
    }
}
