package org.romainb.parisportifapi.entity;

import javax.persistence.*;

@Entity(name="MemberRole")
@Table(name="member_role")
public class MemberRole {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public MemberRole setName(String name) {
        this.name = name;
        return this;
    }
}
