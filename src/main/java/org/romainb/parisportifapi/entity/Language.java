package org.romainb.parisportifapi.entity;

import javax.persistence.*;

@Entity(name="Language")
@Table(name="language")
public class Language {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String country;
    private String code;
    private String dateFormat;
    private String timeFormat;
    private String capitalTimeZone;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Language setName(String name) {
        this.name = name;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Language setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getCode() {
        return code;
    }

    public Language setCode(String code) {
        this.code = code;
        return this;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public Language setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
        return this;
    }

    public String getTimeFormat() {
        return timeFormat;
    }

    public Language setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
        return this;
    }

    public String getCapitalTimeZone() {
        return capitalTimeZone;
    }

    public Language setCapitalTimeZone(String capitalTimeZone) {
        this.capitalTimeZone = capitalTimeZone;
        return this;
    }
}
