package org.romainb.parisportifapi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.*;

@Entity(name="Run")
@Table(name="run")
public class Run {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String event;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date startDate;
    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="competition_id")
    private Competition competition;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="location_id")
    private Location location;
    @JsonManagedReference
    @ManyToMany(targetEntity = Team.class,
            fetch = FetchType.LAZY)
    @JoinTable(name = "run_team",
            joinColumns = @JoinColumn(name = "run_id"),
            inverseJoinColumns = @JoinColumn(name = "team_id"))
    private List<Team> teams;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Run setName(String name) {
        this.name = name;
        return this;
    }

    public String getEvent() {
        return event;
    }

    public Run setEvent(String event) {
        this.event = event;
        return this;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Run setStartDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public Competition getCompetition() {
        return competition;
    }

    public Run setCompetition(Competition competition) {
        this.competition = competition;
        return this;
    }

    public Location getLocation() {
        return location;
    }

    public Run setLocation(Location location) {
        this.location = location;
        return this;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public Run setTeams(List<Team> teams) {
        this.teams = teams;
        return this;
    }
}
