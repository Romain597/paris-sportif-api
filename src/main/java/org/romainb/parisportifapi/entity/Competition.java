package org.romainb.parisportifapi.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import javax.persistence.*;
import java.util.*;

@Entity(name="Competition")
@Table(name="competition")
public class Competition {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date startDate;
    private String country;
    private Integer minRuns = 0;
    private Integer maxRuns = null;
    @JsonManagedReference
    @OneToMany(mappedBy="competition",
            fetch = FetchType.LAZY)
    private List<Run> runs;
    @JsonManagedReference
    @ManyToMany(targetEntity = BetCategory.class,
            fetch = FetchType.LAZY)
    @JoinTable(name = "competition_bet_category",
            joinColumns = @JoinColumn(name = "competition_id"),
            inverseJoinColumns = @JoinColumn(name = "bet_category_id"))
    private List<BetCategory> betCategories;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="sport_id")
    private Sport sport;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Competition setName(String name) {
        this.name = name;
        return this;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Competition setStartDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Competition setCountry(String country) {
        this.country = country;
        return this;
    }

    public Integer getMinRuns() {
        return minRuns;
    }

    public Competition setMinRuns(Integer minRuns) {
        this.minRuns = minRuns;
        return this;
    }

    public Integer getMaxRuns() {
        return maxRuns;
    }

    public Competition setMaxRuns(Integer maxRuns) {
        this.maxRuns = maxRuns;
        return this;
    }

    public List<Run> getRuns() {
        return runs;
    }

    public Competition setRuns(List<Run> runs) {
        this.runs = runs;
        return this;
    }

    public List<BetCategory> getBetCategories() {
        return betCategories;
    }

    public Competition setBetCategories(List<BetCategory> betCategories) {
        this.betCategories = betCategories;
        return this;
    }

    public Sport getSport() {
        return sport;
    }

    public Competition setSport(Sport sport) {
        this.sport = sport;
        return this;
    }
}
