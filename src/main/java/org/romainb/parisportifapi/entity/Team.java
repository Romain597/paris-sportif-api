package org.romainb.parisportifapi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.*;

@Entity(name="Team")
@Table(name="team")
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String country;
    private String odds;
    @JsonManagedReference
    @OneToMany(targetEntity = Member.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name="team_id")
    private List<Member> members;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="sport_id")
    private Sport sport;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Team setName(String name) {
        this.name = name;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Team setCountry(String country) {
        this.country = country;
        return this;
    }

    public List<Member> getMembers() {
        return members;
    }

    public Team setMembers(List<Member> members) {
        this.members = members;
        return this;
    }

    public Sport getSport() {
        return sport;
    }

    public Team setSport(Sport sport) {
        this.sport = sport;
        return this;
    }

    public String getOdds() {
        return odds;
    }

    public Team setOdds(String odds) {
        this.odds = odds;
        return this;
    }
}
