package org.romainb.parisportifapi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.*;

@Entity(name="Wallet")
@Table(name="wallet")
public class Wallet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer amount = 0;
    @OneToOne(mappedBy="wallet",
            fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    @JsonBackReference
    private User user;

    public Integer getId() {
        return id;
    }

    public Integer getAmount() {
        return amount;
    }

    public Wallet setAmount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Wallet setUser(User user) {
        this.user = user;
        return this;
    }
}
