package org.romainb.parisportifapi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity(name="Member")
@Table(name="member")
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String lastName;
    private String firstName;
    private String country;
    private String odds;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="member_role_id")
    private MemberRole memberRole;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="member_status_id")
    private MemberStatus memberStatus;
    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="team_id")
    private Team team;

    public Integer getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public Member setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Member setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Member setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getOdds() {
        return odds;
    }

    public Member setOdds(String odds) {
        this.odds = odds;
        return this;
    }

    public MemberRole getMemberRole() {
        return memberRole;
    }

    public Member setMemberRole(MemberRole memberRole) {
        this.memberRole = memberRole;
        return this;
    }

    public MemberStatus getMemberStatus() {
        return memberStatus;
    }

    public Member setMemberStatus(MemberStatus memberStatus) {
        this.memberStatus = memberStatus;
        return this;
    }

    public Team getTeam() {
        return team;
    }

    public Member setTeam(Team team) {
        this.team = team;
        return this;
    }
}
