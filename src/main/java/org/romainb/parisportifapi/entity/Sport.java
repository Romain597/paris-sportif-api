package org.romainb.parisportifapi.entity;

import javax.persistence.*;

@Entity(name="Sport")
@Table(name="sport")
public class Sport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String country;
    private String runType;
    private boolean individualType = false;
    private boolean collectiveType = false;
    private Integer minTeamsByRun = 0;
    private Integer maxTeamsByRun = null;
    private Integer minMembersByTeam = 0;
    private Integer maxMembersByTeam = null;
    public static final String FIXTURE_TYPE = "fixture";
    public static final String RACE_TYPE = "race";
    public static final String[] RUN_TYPES = { FIXTURE_TYPE, RACE_TYPE };

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Sport setName(String name) {
        this.name = name;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Sport setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getRunType() {
        return runType;
    }

    public Sport setRunType(String runType) {
        this.runType = runType;
        return this;
    }

    public boolean getIndividualType() {
        return individualType;
    }

    public Sport setIndividualType(boolean individualType) {
        this.individualType = individualType;
        return this;
    }

    public boolean getCollectiveType() {
        return collectiveType;
    }

    public Sport setCollectiveType(boolean collectiveType) {
        this.collectiveType = collectiveType;
        return this;
    }

    public Integer getMinTeamsByRun() {
        return minTeamsByRun;
    }

    public Sport setMinTeamsByRun(Integer minTeamsByRun) {
        this.minTeamsByRun = minTeamsByRun;
        return this;
    }

    public Integer getMaxTeamsByRun() {
        return maxTeamsByRun;
    }

    public Sport setMaxTeamsByRun(Integer maxTeamsByRun) {
        this.maxTeamsByRun = maxTeamsByRun;
        return this;
    }

    public Integer getMinMembersByTeam() {
        return minMembersByTeam;
    }

    public Sport setMinMembersByTeam(Integer minMembersByTeam) {
        this.minMembersByTeam = minMembersByTeam;
        return this;
    }

    public Integer getMaxMembersByTeam() {
        return maxMembersByTeam;
    }

    public Sport setMaxMembersByTeam(Integer maxMembersByTeam) {
        this.maxMembersByTeam = maxMembersByTeam;
        return this;
    }
}
