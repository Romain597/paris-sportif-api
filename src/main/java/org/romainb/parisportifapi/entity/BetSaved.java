package org.romainb.parisportifapi.entity;

import javax.persistence.*;
import java.util.*;

@Entity(name="BetSaved")
@Table(name="bet_saved")
public class BetSaved {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String designation;
    private Integer amount;
    private String odds;
    private Integer gains = 0;
    private Boolean isWinning = null;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date betDate;
    private String betCategoryName;
    private String competitionName;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date competitionStartDate;
    private String competitionCountry;
    private String competitionSportName;
    private String competitionSportCountry;
    private String runName = null;
    private String runEvent = null;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date runStartDate = null;
    private String teamName = null;
    private String teamCountry = null;
    private String memberLastName = null;
    private String memberFirstName = null;
    private String memberCountry = null;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    private User user;

    public Integer getId() {
        return id;
    }

    public String getDesignation() {
        return designation;
    }

    public BetSaved setDesignation(String designation) {
        this.designation = designation;
        return this;
    }

    public Integer getAmount() {
        return amount;
    }

    public BetSaved setAmount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public String getOdds() {
        return odds;
    }

    public BetSaved setOdds(String odds) {
        this.odds = odds;
        return this;
    }

    public Integer getGains() {
        return gains;
    }

    public BetSaved setGains(Integer gains) {
        this.gains = gains;
        return this;
    }

    public Boolean getWinning() {
        return isWinning;
    }

    public BetSaved setWinning(Boolean winning) {
        isWinning = winning;
        return this;
    }

    public Date getBetDate() {
        return betDate;
    }

    public BetSaved setBetDate(Date betDate) {
        this.betDate = betDate;
        return this;
    }

    public String getBetCategoryName() {
        return betCategoryName;
    }

    public BetSaved setBetCategoryName(String betCategoryName) {
        this.betCategoryName = betCategoryName;
        return this;
    }

    public String getCompetitionName() {
        return competitionName;
    }

    public BetSaved setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
        return this;
    }

    public Date getCompetitionStartDate() {
        return competitionStartDate;
    }

    public BetSaved setCompetitionStartDate(Date competitionStartDate) {
        this.competitionStartDate = competitionStartDate;
        return this;
    }

    public String getCompetitionCountry() {
        return competitionCountry;
    }

    public BetSaved setCompetitionCountry(String competitionCountry) {
        this.competitionCountry = competitionCountry;
        return this;
    }

    public String getCompetitionSportName() {
        return competitionSportName;
    }

    public BetSaved setCompetitionSportName(String competitionSportName) {
        this.competitionSportName = competitionSportName;
        return this;
    }

    public String getCompetitionSportCountry() {
        return competitionSportCountry;
    }

    public BetSaved setCompetitionSportCountry(String competitionSportCountry) {
        this.competitionSportCountry = competitionSportCountry;
        return this;
    }

    public String getRunName() {
        return runName;
    }

    public BetSaved setRunName(String runName) {
        this.runName = runName;
        return this;
    }

    public String getRunEvent() {
        return runEvent;
    }

    public BetSaved setRunEvent(String runEvent) {
        this.runEvent = runEvent;
        return this;
    }

    public Date getRunStartDate() {
        return runStartDate;
    }

    public BetSaved setRunStartDate(Date runStartDate) {
        this.runStartDate = runStartDate;
        return this;
    }

    public String getTeamName() {
        return teamName;
    }

    public BetSaved setTeamName(String teamName) {
        this.teamName = teamName;
        return this;
    }

    public String getTeamCountry() {
        return teamCountry;
    }

    public BetSaved setTeamCountry(String teamCountry) {
        this.teamCountry = teamCountry;
        return this;
    }

    public String getMemberLastName() {
        return memberLastName;
    }

    public BetSaved setMemberLastName(String memberLastName) {
        this.memberLastName = memberLastName;
        return this;
    }

    public String getMemberFirstName() {
        return memberFirstName;
    }

    public BetSaved setMemberFirstName(String memberFirstName) {
        this.memberFirstName = memberFirstName;
        return this;
    }

    public String getMemberCountry() {
        return memberCountry;
    }

    public BetSaved setMemberCountry(String memberCountry) {
        this.memberCountry = memberCountry;
        return this;
    }

    public User getUser() {
        return user;
    }

    public BetSaved setUser(User user) {
        this.user = user;
        return this;
    }
}
