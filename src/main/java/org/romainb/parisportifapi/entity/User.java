package org.romainb.parisportifapi.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity(name="User")
@Table(name="user")
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String email;
    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String[] roles;
    private String password;
    private String civility = null;
    private String firstName;
    private String lastName;
    private String billingAddress;
    private String billingCity;
    private String billingPostcode;
    private String billingCountry;
    @Temporal(value = TemporalType.DATE)
    private Date birthDate;
    private String timeZoneSelected;
    private boolean deletedStatus;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date deletedDate;
    private boolean suspendedStatus;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date suspendedDate;
    private boolean activatedStatus;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date activatedDate;
    private boolean isVerified = false;
    private boolean newsletters = false;
    private String identityDocument;
    private String residenceProof;
    @OneToMany(targetEntity = Bet.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    @JsonManagedReference
    private List<Bet> onGoingBets;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="wallet_id")
    @JsonManagedReference
    private Wallet wallet;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="language_id")
    private Language language;
    public static final Integer MIN_AGE_FOR_BETTING = 18;
    public static final Integer MAX_AGE_FOR_BETTING = 140;
    public static final String SELECT_CURRENCY_CODE = "EUR";
    public static final String SELECT_CURRENCY_SYMBOL = "€";

    public Integer getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String[] getRoles() {
        return roles;
    }

    public User setRoles(String[] roles) {
        this.roles = roles;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getCivility() {
        return civility;
    }

    public User setCivility(String civility) {
        this.civility = civility;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public User setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public User setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public User setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
        return this;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public User setBillingCity(String billingCity) {
        this.billingCity = billingCity;
        return this;
    }

    public String getBillingPostcode() {
        return billingPostcode;
    }

    public User setBillingPostcode(String billingPostcode) {
        this.billingPostcode = billingPostcode;
        return this;
    }

    public String getBillingCountry() {
        return billingCountry;
    }

    public User setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
        return this;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public User setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public String getTimeZoneSelected() {
        return timeZoneSelected;
    }

    public User setTimeZoneSelected(String timeZoneSelected) {
        this.timeZoneSelected = timeZoneSelected;
        return this;
    }

    public boolean getDeletedStatus() {
        return deletedStatus;
    }

    public User setDeletedStatus(boolean deletedStatus) {
        this.deletedStatus = deletedStatus;
        return this;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public User setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
        return this;
    }

    public boolean getSuspendedStatus() {
        return suspendedStatus;
    }

    public User setSuspendedStatus(boolean suspendedStatus) {
        this.suspendedStatus = suspendedStatus;
        return this;
    }

    public Date getSuspendedDate() {
        return suspendedDate;
    }

    public User setSuspendedDate(Date suspendedDate) {
        this.suspendedDate = suspendedDate;
        return this;
    }

    public boolean getActivatedStatus() {
        return activatedStatus;
    }

    public User setActivatedStatus(boolean activatedStatus) {
        this.activatedStatus = activatedStatus;
        return this;
    }

    public Date getActivatedDate() {
        return activatedDate;
    }

    public User setActivatedDate(Date activatedDate) {
        this.activatedDate = activatedDate;
        return this;
    }

    public List<Bet> getOnGoingBets() {
        return onGoingBets;
    }

    public User setOnGoingBets(List<Bet> onGoingBets) {
        this.onGoingBets = onGoingBets;
        return this;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public User setWallet(Wallet wallet) {
        this.wallet = wallet;
        return this;
    }

    public boolean getVerified() {
        return isVerified;
    }

    public User setVerified(boolean verified) {
        isVerified = verified;
        return this;
    }

    public boolean getNewsletters() {
        return newsletters;
    }

    public User setNewsletters(boolean newsletters) {
        this.newsletters = newsletters;
        return this;
    }

    public String getIdentityDocument() {
        return identityDocument;
    }

    public User setIdentityDocument(String identityDocument) {
        this.identityDocument = identityDocument;
        return this;
    }

    public String getResidenceProof() {
        return residenceProof;
    }

    public User setResidenceProof(String residenceProof) {
        this.residenceProof = residenceProof;
        return this;
    }

    public Language getLanguage() {
        return language;
    }

    public User setLanguage(Language language) {
        this.language = language;
        return this;
    }
}
