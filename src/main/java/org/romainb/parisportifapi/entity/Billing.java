package org.romainb.parisportifapi.entity;

import javax.persistence.*;
import java.util.*;

@Entity(name="Billing")
@Table(name="billing")
public class Billing {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String postcode;
    private String country;
    private String designation;
    private Integer orderNumber;
    private Integer invoiceNumber;
    private Integer amount;
    private String commissionRate;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date issueDate;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date deliveryDate;
    private String operationType;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    private User user = null;
    public static final Double DEFAULT_COMMISSION_RATE =  7.5;
    public static final String DEFAULT_CURRENCY_CODE = "EUR";
    public static final String DEFAULT_CURRENCY_SYMBOL = "€";
    public static final String CREDIT = "credit";
    public static final String DEBIT = "debit";
    public static final String[] OPERATION_TYPES = { DEBIT, CREDIT };

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Billing setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Billing setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Billing setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Billing setCity(String city) {
        this.city = city;
        return this;
    }

    public String getPostcode() {
        return postcode;
    }

    public Billing setPostcode(String postcode) {
        this.postcode = postcode;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Billing setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getDesignation() {
        return designation;
    }

    public Billing setDesignation(String designation) {
        this.designation = designation;
        return this;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public Billing setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    public Integer getInvoiceNumber() {
        return invoiceNumber;
    }

    public Billing setInvoiceNumber(Integer invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
        return this;
    }

    public Integer getAmount() {
        return amount;
    }

    public Billing setAmount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public String getCommissionRate() {
        return commissionRate;
    }

    public Billing setCommissionRate(String commissionRate) {
        this.commissionRate = commissionRate;
        return this;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public Billing setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
        return this;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public Billing setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public Billing setOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Billing setUser(User user) {
        this.user = user;
        return this;
    }
}
