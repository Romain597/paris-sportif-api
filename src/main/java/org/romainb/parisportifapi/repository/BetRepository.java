package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.Bet;
import org.springframework.data.repository.CrudRepository;

public interface BetRepository extends CrudRepository<Bet, Integer> {
}
