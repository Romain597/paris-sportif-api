package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.Billing;
import org.springframework.data.repository.CrudRepository;

public interface BillingRepository extends CrudRepository<Billing, Integer> {
}
