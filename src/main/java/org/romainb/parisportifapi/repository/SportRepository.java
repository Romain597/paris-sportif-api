package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.Sport;
import org.springframework.data.repository.CrudRepository;

public interface SportRepository extends CrudRepository<Sport, Integer> {
}
