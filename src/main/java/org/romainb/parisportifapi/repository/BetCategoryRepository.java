package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.BetCategory;
import org.springframework.data.repository.CrudRepository;

public interface BetCategoryRepository extends CrudRepository<BetCategory, Integer> {
}
