package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.Member;
import org.springframework.data.repository.CrudRepository;

public interface MemberRepository extends CrudRepository<Member, Integer> {
}
