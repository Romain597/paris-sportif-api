package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.MemberRole;
import org.springframework.data.repository.CrudRepository;

public interface MemberRoleRepository extends CrudRepository<MemberRole, Integer> {
}
