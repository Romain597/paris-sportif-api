package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.Language;
import org.springframework.data.repository.CrudRepository;

public interface LanguageRepository extends CrudRepository<Language, Integer> {
}
