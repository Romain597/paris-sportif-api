package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.MemberStatus;
import org.springframework.data.repository.CrudRepository;

public interface MemberStatusRepository extends CrudRepository<MemberStatus, Integer> {
}
