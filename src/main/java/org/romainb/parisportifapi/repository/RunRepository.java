package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.Run;
import org.springframework.data.repository.CrudRepository;

public interface RunRepository extends CrudRepository<Run, Integer> {
}
