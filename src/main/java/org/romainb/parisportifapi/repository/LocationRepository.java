package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.Location;
import org.springframework.data.repository.CrudRepository;

public interface LocationRepository extends CrudRepository<Location, Integer> {
}
