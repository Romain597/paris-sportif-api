package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.Wallet;
import org.springframework.data.repository.CrudRepository;

public interface WalletRepository extends CrudRepository<Wallet, Integer> {
}
