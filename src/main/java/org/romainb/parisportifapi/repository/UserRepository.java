package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
}
