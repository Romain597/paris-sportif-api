package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.Competition;
import org.springframework.data.repository.CrudRepository;

public interface CompetitionRepository extends CrudRepository<Competition, Integer> {
}
