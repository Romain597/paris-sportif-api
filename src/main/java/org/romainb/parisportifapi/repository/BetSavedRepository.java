package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.BetSaved;
import org.springframework.data.repository.CrudRepository;

public interface BetSavedRepository extends CrudRepository<BetSaved, Integer> {
}
