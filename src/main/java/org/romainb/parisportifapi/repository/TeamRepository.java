package org.romainb.parisportifapi.repository;

import org.romainb.parisportifapi.entity.Team;
import org.springframework.data.repository.CrudRepository;

public interface TeamRepository extends CrudRepository<Team, Integer> {
}
